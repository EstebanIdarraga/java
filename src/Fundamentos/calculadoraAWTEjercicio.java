/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fundamentos;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.TextField;
import java.awt.Event;


/**
 *
 * @author Sena
 */
public class calculadoraAWTEjercicio extends Applet{
  
  Button btMc,btMr,btMs,btDelete,btCe,btC,btMasMenos,btRaiz,btDiv,btPorcentaje,btMulti,btResta,btSuma,btIgual,btX,btComa;
  Button bt0,bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9;
  TextField txt1,txt2,txt3;
  int a,b,x,c,op,cont=1,a1,a2,o,igual,Ms;
  String con,con1;

  
  public void init(){
       setBackground(Color.gray);
    btMc = new Button("MC");
    btMr = new Button("MR");
    btMs = new Button("MS");
    
    btDelete = new Button("<=");
    btCe = new Button("CE");
    btC = new Button("C");
    btMasMenos = new Button("+/-");
    btRaiz = new Button("√");
    btDiv = new Button("/");
    btPorcentaje = new Button("%");
    btX = new Button("1/x");
    btMulti = new Button("*");
    btResta = new Button("-");
    btSuma = new Button("+");
    btIgual = new Button("=");
    btComa = new Button(",");
    
    bt0 = new Button("0");
    bt1 = new Button("1");
    bt2 = new Button("2");
    bt3 = new Button("3");
    bt4 = new Button("4");
    bt5 = new Button("5");
    bt6 = new Button("6");
    bt7 = new Button("7");
    bt8 = new Button("8");
    bt9 = new Button("9");
    
    txt1 = new TextField();
    txt2 = new TextField();
    txt3 = new TextField();

    this.add(btMc);
    this.add(btMr);
    this.add(btMs);
    this.add(btDelete);
    this.add(btCe);
    this.add(btC);
    this.add(btMasMenos);
    this.add(btRaiz);
    this.add(btDiv);
    this.add(btPorcentaje);
    this.add(btMulti);
    this.add(btResta);
    this.add(btSuma);
    this.add(btIgual);
    this.add(btX);
    this.add(btComa);
    
    this.add(bt0);
    this.add(bt1);
    this.add(bt2);
    this.add(bt3);
    this.add(bt4);
    this.add(bt5);
    this.add(bt6);
    this.add(bt7);
    this.add(bt8);
    this.add(bt9);
    
    
    this.add(txt1);
    this.add(txt2);
    this.add(txt3);
    this.resize(500,500);  
  }
  
  
  public void paint(Graphics g){
    
      txt1.setBounds(100,100,290,80);
      txt2.setBounds(400,100,40,20);
      txt3.setBounds(100,80,290,20);
      btMc.setBounds(100,200,90,40);
      btMr.setBounds(200,200,90,40);
      btMs.setBounds(300,200,90,40);
      
      btDelete.setBounds(100,250,50,40);
      btCe.setBounds(160,250,50,40);
      btC.setBounds(220,250,50,40);
      btMasMenos.setBounds(280,250,50,40);
      btRaiz.setBounds(340,250,50,40);
      
      bt7.setBounds(100,300,50,40);
      bt8.setBounds(160,300,50,40);
      bt9.setBounds(220,300,50,40);
      btDiv.setBounds(280,300,50,40);
      btPorcentaje.setBounds(340,300,50,40);
      
      bt4.setBounds(100,350,50,40);
      bt5.setBounds(160,350,50,40);
      bt6.setBounds(220,350,50,40);
      btMulti.setBounds(280,350,50,40);
      btX.setBounds(340,350,50,40);
      
      bt1.setBounds(100,400,50,40);
      bt2.setBounds(160,400,50,40);
      bt3.setBounds(220,400,50,40);
      btResta.setBounds(280,400,50,40);
      btIgual.setBounds(340,400,50,90);
      
      bt0.setBounds(100,450,110,40);
      btComa.setBounds(220,450,50,40);
      btSuma.setBounds(280,450,50,40);
  }

public void selectNumA(){

  switch(op){
    case 1:   
      con=txt1.getText()+1;
      break;
    case 2:
      
      a = 2;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 3:
      a = 3;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 4:
      a = 4;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 5:
      a = 5;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 6:
      a = 6;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 7:
      a = 7;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 8:
      a = 8;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 9:
      a = 9;
      con=txt1.getText()+String.valueOf(a);
      break;
    case 10:
      a = 0;
      con=txt1.getText()+String.valueOf(a);
      break;    
  }
  txt1.setText(String.valueOf(con));
}

public void selectNumB(){

  switch(op){
    case 1:   
      b = 1;
      con1=txt1.getText()+1;
      break;
    case 2:
      
      b = 2;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 3:
      b = 3;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 4:
      b = 4;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 5:
      b = 5;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 6:
      b = 6;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 7:
      b = 7;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 8:
      b = 8;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 9:
      b = 9;
      con1=txt1.getText()+String.valueOf(b);
      break;
    case 10:
      b = 0;
      con1=txt1.getText()+String.valueOf(b);
      break;
      
  }
 txt1.setText(String.valueOf(con1));
}

 public boolean action(Event e, Object o){
    if(e.target.equals(bt0)){
      if(cont==1){
           op=10;
           selectNumA();
      }else if(cont==2){
           op=10;
           selectNumB();
      
      } 
    }
     
    if(e.target.equals(bt1)){
       if(cont==1){
        op=1;
        selectNumA();
       }else if(cont==2){
        op=1;
        selectNumB();
       }
    }
    
    if(e.target.equals(bt2)){
        if(cont==1){
        op=2;
        selectNumA();
        }else if(cont==2){
         op=2;
         selectNumB();
        }
    }
    
    
    if(e.target.equals(bt3)){
   if(cont==1){
        op=3;
        selectNumA();
        }else if(cont==2){
         op=3;
         selectNumB();
        }

    }
    if(e.target.equals(bt4)){
    if(cont==1){
        op=4;
        selectNumA();
        }else if(cont==2){
         op=4;
         selectNumB();
        }
    }
    
    if(e.target.equals(bt5)){
    if(cont==1){
        op=5;
        selectNumA();
        }else if(cont==2){
         op=5;
         selectNumB();
        }
    }
    if(e.target.equals(bt6)){
    if(cont==1){
        op=6;
        selectNumA();
        txt3.setText(String.valueOf(""));
        }else if(cont==2){
         op=6;
         selectNumB();
        }
    }
       
    if(e.target.equals(bt7)){
    if(cont==1){
        op=7;
        selectNumA();
        }else if(cont==2){
         op=7;
         selectNumB();
        }
    }
    if(e.target.equals(bt8)){
  if(cont==1){
        op=8;
        selectNumA();
        }else if(cont==2){
         op=8;
         selectNumB();
        }
    }
    
    if(e.target.equals(bt9)){
  if(cont==1){
        op=9;
        selectNumA();
        }else if(cont==2){
         op=9;
         selectNumB();
        }
    }   
    //OPERACIONES 
    
    if(e.target.equals(btSuma)){
     x=1;
    txt1.setText(String.valueOf(""));
    cont=cont+1;
    txt2.setText(String.valueOf("+"));
    }
    
    if(e.target.equals(btResta)){
    x=2;
    txt1.setText(String.valueOf(""));
    cont=cont+1;
    txt2.setText(String.valueOf("-"));
    }
    
   if(e.target.equals(btMulti)){
   x=3;
   txt1.setText(String.valueOf(""));
   cont=cont+1;
   txt2.setText(String.valueOf("*"));
   }
   
   if(e.target.equals(btDiv)){
   x=4;
    txt1.setText(String.valueOf(""));
    cont=cont+1;
   txt2.setText(String.valueOf("/"));        
   }

 //GUARDAR EN MEMORIA
  
   if(e.target.equals(btMc)){
   x=5;
   operar();
   
   txt2.setText(String.valueOf("MC"));
   }
   if(e.target.equals(btMr)){
   x=6;
   operar();
   txt2.setText(String.valueOf("MR"));
   }
   if(e.target.equals(btMs)){
   x=7;
   operar();
   txt2.setText(String.valueOf("MS"));
   }
   
   
    
   if(e.target.equals(btDelete)){
   x=8;
   }
   
    if(e.target.equals(btIgual)){   
       operar();
       resultado();
    }  
    return true;
}

public void operar(){
  a1 = Integer.parseInt(con);
  a2 = Integer.parseInt(con1);
    switch(x){
        case 1:
            c = a1 + a2 ; 
            break;
        case 2:
           c = a1 - a2;
           break;
        case 3:
           c = a1 * a2;
           break;
        case 4:
           c = a1 / a2;
           break;
        case 5:
          txt1.setText(String.valueOf(""));
          txt2.setText(String.valueOf(""));
          Ms=0;
          break;
        case 6:
            txt1.setText(String.valueOf(""));
            a1=Ms;
          break;
        case 7:
          Ms = a1;

          break; 
        case 8:
          break;
           
    }
}

public void resultado(){
  txt1.setText(String.valueOf(""));
  txt2.setText(String.valueOf(""));
  txt3.setText(String.valueOf(c));
  
  a1=0;
  a2=0;
  cont=1;
}
}

 
  