/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fundamentos;

import java.applet.Applet;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Image;

/**
 *
 * @author Sena
 */
public class JuegoFinal extends Applet{
  int laberinto[][]={
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,1,1,1,0,0,0,0,1,1,1,1,1,1,0},
    {0,1,1,1,1,1,1,1,1,0,0,1,0,0,0},
    {0,0,0,0,0,0,0,1,1,0,0,1,1,1,0},
    {0,0,1,0,1,1,1,1,1,1,1,1,0,0,0},
    {0,0,1,0,0,1,0,0,0,0,0,0,0,0,0},
    {0,1,1,1,1,1,0,0,0,0,0,0,0,0,0},     
    {0,1,0,0,0,0,0,0,0,1,1,1,1,1,0},
    {0,1,1,1,1,1,1,1,0,1,0,1,0,0,0},
    {0,1,0,0,1,0,0,1,1,1,0,1,0,0,0},
    {0,1,0,0,1,0,0,1,0,0,0,1,1,0,0},
    {0,1,0,0,1,1,1,1,0,0,0,1,1,1,0},
    {0,1,0,0,1,0,0,1,0,0,0,1,0,1,0},
    {0,1,0,0,1,0,0,1,1,1,0,1,0,1,1},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}         
  };
  
  
  Image muro,camino,arriba,abajo,derecha,izquierda;
  int x=40,y=40,i=0,j=0,mover,l=1,o=1;
  
  public void init(){
  muro = getImage(getDocumentBase(),"muro.jpg");
  camino = getImage(getDocumentBase(),"camino.jpg");
  arriba = getImage(getDocumentBase(),"atras.jpg");
  abajo = getImage(getDocumentBase(),"frente.jpg");
  derecha = getImage(getDocumentBase(),"derecha.jpg");
  izquierda = getImage(getDocumentBase(),"izquierda.jpg");
  
  this.resize(500,500);
  }
  
  public void paint(Graphics g){
 
    for(i=0;i<15;i++){
      for(j=0;j<15;j++){
        if(laberinto[i][j]==0){
          g.drawImage(muro,j*40,i*40,40,40,this);       
        }else if(laberinto[i][j]==1){
          g.drawImage(camino,j*40,i*40,40,40,this);
        }
      } 
    }        
    g.drawImage(abajo,x,y,40,40,this);
    personaje(g);
  }
  
  public void personaje(Graphics g){
  switch(mover){
    case 1:g.drawImage(arriba,x,y,40,40,this);break;
    case 2:g.drawImage(abajo,x,y,40,40,this);break;
    case 3:g.drawImage(derecha,x,y,40,40,this);break;
    case 4:g.drawImage(izquierda,x,y,40,40,this);break;  
  }
  }
  
  public boolean keyDown(Event e,int key){   
  if(key==e.UP){
  mover=1;
  if(laberinto[o-1][l]!=0){
  y=y-40;
  o=o-1;
  }
  }else if(key==e.DOWN){
  mover=2;
  if(laberinto[o+1][l]!=0){
  y=y+40;
  o=o+1;
  }
  }else if(key==e.RIGHT){
  mover=3;
  x=x+40;
  
  }else if(key==e.LEFT){
  mover=4;
  x=x-40;
  } 
  repaint();
  return true;
  
  }
  
  
}
