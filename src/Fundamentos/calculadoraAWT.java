/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fundamentos;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.TextField;

/**
 *
 * @author Sena
 */
public class calculadoraAWT extends Applet{
  Button btSuma,btResta,btMulti,btDivicion;
  TextField txt1,txt2,txtRes;
  int a,b,c,op;
  
  public void init(){
  btSuma = new Button("+");
  btResta = new Button("-");
  btMulti = new Button("*");
  btDivicion = new Button("/");
  
  txt1 = new TextField();
  txt2 = new TextField();
  txtRes = new TextField();
  

  this.add(btSuma);
  this.add(btResta);
  this.add(btMulti);
  this.add(btDivicion);
  this.add(txt1);
  this.add(txt2);
  this.add(txtRes);
  this.resize(500,500);
  }
  
  
  public void paint(Graphics g){
    txt1.setBounds(150,150,60,60);
    txt2.setBounds(250,150,60,60);
    txtRes.setBounds(350,150,80,30);
    
    
    btSuma.setBounds(150,250,40,40);
    btResta.setBounds(250,250,40,40);
    btMulti.setBounds(150,350,40,40);
    btDivicion.setBounds(250,350,40,40);
  }
  
  public void asignar(){
  a=Integer.parseInt(txt1.getText());
  b=Integer.parseInt(txt2.getText());
  
  }
  
  public void operaciones(){
  switch(op){
    case 1: c=a+b;
    break;
    
    case 2: c=a-b;
    break;
    
    case 3: c=a*b;
    break;
    
    case 4: c=a/b;
    break;
      
  }
  txtRes.setText(String.valueOf(c));
  }
  

  public boolean action(Event e, Object o){
    if(e.target.equals(btSuma)){
    op=1;
    asignar();
    operaciones();
    }
    
    if(e.target.equals(btResta)){
    op=2;
    asignar();
    operaciones();
    }
    
    if(e.target.equals(btMulti)){
    op=3;
    asignar();
    operaciones();
    }
    
    if(e.target.equals(btDivicion)){
    op=4;
    asignar();
    operaciones();
    }
    
    return true;
  }
  
}
