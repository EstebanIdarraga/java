/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fundamentos;

import java.util.Scanner;

/**
 *
 * @author Sena
 */
public class AreaTriangulo {
  
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
   
    int a,b,c;
    System.out.println(">>> Calculador el Area de un Triangulo <<<");
    System.out.println("Ingrese el valor de la base:");
    a =sc.nextInt();
    
    System.out.println("Ingrese el valor de la altura:");
    b= sc.nextInt();
    
    c=a*b/2;
    System.out.println("El area es igual a "+c);
  
  }
  
}
